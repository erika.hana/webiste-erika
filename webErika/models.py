from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.


class Schedule(models.Model):
    course = models.CharField(max_length=100, null=True)
    lecturer = models.CharField(max_length=100, null=True)
    SKSNumber = models.CharField(max_length=2, null=True)
    description = models.CharField(max_length=300, null=True)
    batch = models.CharField(max_length=20, null=True)
    classroomNumber = models.CharField(max_length=30, null=True)
    dateCreated = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.course
