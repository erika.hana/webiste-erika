from django.contrib import admin
from django.urls import path
from . import views

app_name = 'webErika'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('about/', views.about, name='about'),
    path('experiences/', views.experiences, name='experiences'),
    path('projects/', views.projects, name='projects'),
    path('forFun/', views.forFun, name='forFun'),

    path('scheduleList/', views.scheduleIndex, name='scheduleList'),
    path('addSchedule/', views.addSchedule, name='addSchedule'),  # udh oke
    path('scheduleDetails/<int:id>', views.scheduleDetails, name='scheduleDetails'),
    path('scheduleDelete/<int:id>', views.removeSchedule, name="removeSchedule"),

    path('schedule/', views.readSchedule, name='schedule'),
    path('forErika/', views.forErika, name='forErika'),
    path('contact/', views.contact, name='contact'),
    path('thanks/', views.thanks, name='thanks'),
    # dilanjutkan ...
]
