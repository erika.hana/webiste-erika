# Generated by Django 3.1.1 on 2020-10-17 04:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webErika', '0005_auto_20201016_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='SKSNumber',
            field=models.IntegerField(null=True),
        ),
    ]
