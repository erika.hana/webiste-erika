from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm

# Create your views here.


def homepage(request):
    return render(request, 'homepage.html')


def about(request):
    return render(request, 'about.html')


def experiences(request):
    return render(request, 'experiences.html')


def projects(request):
    return render(request, 'projects.html')


def forFun(request):
    return render(request, 'forFun.html')


def scheduleIndex(request, *args, **kwargs):
    form = ScheduleForm(Schedule)
    schedule = Schedule.objects.all()

    context = {
        'form': form,
        'schedule': schedule
    }
    return render(request, 'scheduleList.html', context)


def addSchedule(request):
    form = ScheduleForm()
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            #print('Printing POST:', request.POST)
            form.save()
            return redirect('webErika:scheduleList')
        else:
            return HttpResponse('Error when saving')
    context = {'form': form}
    return render(request, 'addSchedule.html', context)


def removeSchedule(request, id, *args, **kwargs):
    Schedule.objects.filter(id=id).delete()
    return redirect('webErika:scheduleList')


def scheduleDetails(request, id):
    schedule = Schedule.objects.get(id=id)
    context = {'schedule': schedule}
    return render(request, 'scheduleDetails.html', context)


def readSchedule(request):
    schedule = Schedule.objects.all()
    return render(request, 'schedule.html', {'schedule': schedule})


def listMatkul(request):
    return render(request, 'listMatkul.html')


def forErika(request):
    return render(request, 'forErika.html')


def contact(request):
    return render(request, 'contact.html')


def thanks(request):
    return render(request, 'thanks.html')
